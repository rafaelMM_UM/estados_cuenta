<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Contracts\View\View;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{

    public function index(){
        $campuses = DB::table('campuses')->select('id','name')->get();

        return view('welcome',compact('campuses'));
    }

    public function getStudentAlterna(Request $request){
        $alterna = trim($request->input('alterna'));
        $campus = trim($request->input('campus'));

        $student = DB::select("select Distinct st.name, st.last_name, st.second_lastname from all_inscriptions ai, students st where ai.bank_number like '%$alterna%' and ai.campus_id = '$campus' and ai.student_id = st.id");

        return $student;
    }

    public function getdatastudent(Request $request){

        $name = strtoupper($request->input('name'));
        $last_name = strtoupper($request->input('last_name'));
        $second_lastname = strtoupper($request->input('second_lastname'));
        $campus = trim($request->input('campus'));

        $students = DB::select("select distinct on (students.id) students.*, all_inscriptions.bank_number from students,all_inscriptions where name like '%$name%' AND last_name like '%$last_name%' and second_lastname like '%$second_lastname%' and students.campus_id = '$campus' and students.id = all_inscriptions.student_id");
        
        return $students;
    }

    public function getstudent_leave_school(Request $request){

        $id = trim($request->input('id'));

        $leaves = DB::select(" select id, student_id, date_desertion, leave_school_id, comments, status, cycle_id, group_id, career_id, grade, type_income, mode
        FROM public.student_leave_school where student_id = '$id'");

        return $leaves;

    }

    public function getpayments(Request $request){
        $student_id = trim($request->input('matricula'));
        $campus =  trim($request->input('campus'));

        $payments = DB::select("SELECT cicloa, subcicloa, matricula, date, hora, concepto, subconcepto, tipo, formapago, referencia, descuento, amount, recargo, pagocargo, pagorecargo, matriculafam, recibo, cajero, impuestocargo, impuestorecargo, pagoimpuestocargo, pagoimpuestorecargo, fechadeposito, comision, pagocomision, asignatura, examen, id_factconcentrada, no_control, factanticipada, id_cnx_pagos, numctapago, foliobancario, bancometodopago, nr, abono_a_nocontrol, descripcioncorta, campus, ciclo, student_id, campust, concepto_id, subconcepto_id, concept_assign_id, subconcept_assign_id, grupo, seccion, group_id, career_id, payment_method_id, description, adjustment_description, transfer_description, invoice_id, reference, created_at, updated_at, status, payments_temporal, id
        FROM public.payments_temporal where matricula = '$student_id' and campus ='$campus' order by cicloa, subcicloa,date,concepto asc");

        return $payments;
    }

    public function getConcetps(Request $request){

        $id = trim($request->input('id'));
        $student_id = trim($request->input('matricula'));
        $campus =  trim($request->input('campus'));

        $totalConcepts = array();

        $payments = DB::select("SELECT distinct cicloa, subcicloa, concepto
        FROM public.payments_temporal where matricula = '$student_id' and campus ='$campus' order by cicloa, subcicloa asc");

        $concepts = DB::select("select
        distinct
        cycles.cycle_year,
        cycles.cycle_quarter,
        collection_sub_assings_temporal.key,
        collection_sub_assings_temporal.clave,
        collection_sub_concepts_temporal.to_import
        from
        cycles,
        collection_sub_assings_temporal,
        collection_sub_concepts_temporal,
        all_inscriptions,
        students
        where all_inscriptions.group_id=cast(collection_sub_assings_temporal.group_id as int4)
        and all_inscriptions.grade=cast(collection_sub_assings_temporal.grade as int4)
        and all_inscriptions.mode=collection_sub_assings_temporal.mode
        and all_inscriptions.career_id=collection_sub_assings_temporal.career_id
        and all_inscriptions.cycle_id=collection_sub_assings_temporal.cycle_id
        and cycles.id = collection_sub_assings_temporal.cycle_id
        and collection_sub_concepts_temporal.id = collection_sub_assings_temporal.collection_subconcept_id
        and students.id = all_inscriptions.student_id
        and all_inscriptions.student_id= '$id'
				UNION
        select
        cycles.cycle_year,
        cycles.cycle_quarter,
        collection_sub_assings_temporal.key,
        collection_sub_assings_temporal.clave,
        collection_sub_concepts_temporal.to_import
        from
        collection_sub_assings_temporal,
        cycles,
        collection_sub_concepts_temporal
        where collection_sub_assings_temporal.cycle_id = cycles.id
        and collection_sub_concepts_temporal.id = collection_sub_assings_temporal.collection_subconcept_id
        and student_id='$id'
				
       order by cycle_year,cycle_quarter asc;");


       for($i = 0; $i < count($payments); $i ++){
           $concepto = $payments[$i]->concepto;
           $ciclo = $payments[$i]->cicloa;
           $subciclo = $payments[$i]->subcicloa;

           $conceptsByGroup = DB::select("SELECT DISTINCT
           collections_concepts.ciclo,
           collections_concepts.subciclo,
           collections_concepts.key,
           collection_sub_concepts.clave,
           collection_sub_concepts.to_import
           FROM
           collections_concepts,
           collection_sub_concepts
           where collections_concepts.id = collection_sub_concepts.collection_concept_id
           and collections_concepts.key = '$concepto' and collections_concepts.ciclo = $ciclo and collections_concepts.subciclo = $subciclo");

           $totalConcepts =array_merge($totalConcepts, $conceptsByGroup);
       }

       $totalConcepts = array_merge($totalConcepts,$concepts);

        return $totalConcepts;
    }

    public function getBecasIndv(Request $request){
        $student_id = trim($request->input('matricula'));
        $campus =  trim($request->input('campus'));

        $becasIndv = DB::select("select
        asignacion_beca.cicloa,
        asignacion_beca.subcicloa,
        asignacion_beca.beca,
        scholarships.concept,
        collection_sub_concepts.key,
        collection_sub_concepts.clave,
        scholarship_subconcepts.discount
        --asignacion_beca.fecha
        from
        asignacion_beca,
        scholarships,
        scholarship_subconcepts,
        collection_sub_concepts
        where asignacion_beca.matricula = '$student_id'
        and asignacion_beca.campus = '$campus'
        and asignacion_beca.cicloa = scholarships.ciclo
        and asignacion_beca.subcicloa = scholarships.subciclo
        and asignacion_beca.beca = scholarships.key
        and scholarship_subconcepts.scholarship_id = scholarships.id
        and collection_sub_concepts.id = scholarship_subconcepts.collection_subconcept_id");

        return $becasIndv;
    }

    public function getBecasGroup(Request $request){

        $campus =  trim($request->input('campus'));
        $student_id = trim($request->input('matricula'));
        $totalBecas = array();

        $payments = DB::select("SELECT distinct cicloa, subcicloa, grupo
        FROM public.payments_temporal where matricula = '$student_id' and campus ='$campus' order by cicloa, subcicloa asc");

        for($i = 0; $i < count($payments); $i ++){
            $grupo = $payments[$i]->grupo;
            $ciclo = $payments[$i]->cicloa;
            $subciclo = $payments[$i]->subcicloa;

            $becasGroup = DB::select("select
                asignacion_beca.cicloa,
                asignacion_beca.subcicloa,
                asignacion_beca.beca,
                scholarships.concept,
                collection_sub_concepts.key,
                collection_sub_concepts.clave,
                scholarship_subconcepts.discount,
                asignacion_beca.fecha
                from
                asignacion_beca,
                scholarships,
                scholarship_subconcepts,
                collection_sub_concepts
                where asignacion_beca.grupo = '$grupo' and asignacion_beca.cicloa = '$ciclo' and asignacion_beca.subcicloa = '$subciclo'
                and asignacion_beca.campus = '$campus'
                and asignacion_beca.cicloa = scholarships.ciclo
                and asignacion_beca.subcicloa = scholarships.subciclo
                and asignacion_beca.beca = scholarships.key
                and scholarship_subconcepts.scholarship_id = scholarships.id
                and collection_sub_concepts.id = scholarship_subconcepts.collection_subconcept_id"); 

            $totalBecas =array_merge($totalBecas, $becasGroup);
        }

        return $totalBecas;
    }

}