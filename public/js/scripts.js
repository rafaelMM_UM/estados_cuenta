$( document ).ready(function() {

    $("#search").on('click', function(){
         var name = $("#name").val();
         var last_name = $("#last_name").val();
         var second_lastname = $("#second_lastname").val();
         var campus = $("#campus_id").val();
         $.ajax({
             url: '/getdatastudent',
             type: 'POST',
             dataType: 'json',
             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             data: {name: name, last_name: last_name,second_lastname: second_lastname, campus: campus},
         })
         .done(function(data) {
             $("#resultDataStudent").empty();
             console.log(data)

             if(data.length){
                for (var i = 0; i < data.length; i++) {
                $("#resultDataStudent").append('<tr> <td> '+data[i].bank_number+ '</td> <td> '+data[i].student_id+ '</td> <td> '+data[i].name+ '</td> <td> '+data[i].last_name+ '</td> <td> '+data[i].second_lastname+ '</td> <td> '+data[i].campus+ '</td>                                                                                                                         <td><input type="button" value="Bajas" class="btn btn-danger" onclick="student_leave_school('+data[i].id+' , '+data[i].student_id+')" >                                      <input type="button" value="Pagos" class="btn btn-success" onclick="payments('+data[i].student_id+' , '+data[i].campus_id+')" >                                            <input type="button" value="Conceptos" class="btn btn-info" onclick="concepts('+data[i].id+', '+data[i].student_id+' , '+data[i].campus_id+')" >                            <input type="button" value="Becas Individuales" class="btn btn-primary" onclick="becasIndv('+data[i].student_id+','+data[i].campus_id+')" >                                  <input type="button" value="Becas Grupales" class="btn btn-primary" onclick="becasGroup('+data[i].student_id+','+data[i].campus_id+')" > </td></tr>');
                }
             }else{
               alert("No se encontraron datos para el alumno: " + name+ " " +last_name+ " " +second_lastname);
             }
         })
         .fail(function() {
             console.log("error");
         });

    });

    $("#searchAlterna").on('click', function(){

        var alterna = $("#alterna").val();
        var campus = $("#campus_alterna").val();

        if(alterna.length < 5)
            alert("ingrese al menos 5 digitos para buscar");
        else{
            $.ajax({
                url: '/getstudentalterna',
                type: 'POST',
                dataType: 'json',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {alterna: alterna, campus: campus},
            })
            .done(function(data) {
                $("#resultStudent").empty();
    
                if(data.length){
                   for (var i = 0; i < data.length; i++) {
                   $("#resultStudent").append('<tr> <td> '+data[i].name+ '</td> <td> '+data[i].last_name+ '</td> <td> '+data[i].second_lastname+ '</td> <tr>');
                   }
                }else{
                  alert("No se encontraron datos para la alterna : " +alterna);
                }
            })
            .fail(function() {
                console.log("error");
            });
        }

    })

 });

 function student_leave_school(id, matricula){
         $.ajax({
             url: '/getstudent_leave_school',
             type: 'POST',
             dataType: 'json',
             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             data: {id: id, matricula: matricula},
         })
         .done(function(data) {

            $(".modal-dialog").css("max-width","500px");		
            $("#headers, #dataColumns, #title-modal ").empty();

            $("#title-modal").text("Fechas de deserción");

            if(data.length){
                $("#headers").append('<th width="25%"> Fecha de Baja </th>');
                $("#headers").append('<th width="75%"> Comentarios </th>');


                for (var i = 0; i < data.length; i++) {

                    $("#dataColumns").append('<tr><td> '+data[i].date_desertion+ '</td> <td> '+data[i].comments+ '</td> </tr>');
   
                }
            
                $('#exampleModal').modal({
                    backdrop: 'static'
                  });

             }else{
               alert("No se encontraron datos de bajas");
             }

            
         })
         .fail(function() {
             console.log("error");
         });
    }

 function payments(matricula,campus){
     $.ajax({
             url: '/getpayments',
             type: 'POST',
             dataType: 'json',
             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             data: {matricula: matricula, campus: campus},
         })
         .done(function(data) {
            $(".modal-dialog").css("max-width","100%");								
            $("#headers, #dataColumns, #title-modal ").empty();

            $("#title-modal").text("Pagos");

            if(data.length){
                $("#headers").append('<th> ciclo </th>');
                $("#headers").append('<th> subciclo </th>');
                $("#headers").append('<th> matricula </th>');
                $("#headers").append('<th> Fecha Pago </th>');

                $("#headers").append('<th> Concepto </th>');
                $("#headers").append('<th> Subconcepto </th>');
                $("#headers").append('<th> Tipo </th>');
                $("#headers").append('<th> Forma Pago </th>');
                $("#headers").append('<th> Referencia </th>');

                $("#headers").append('<th> Cargo </th>');
                $("#headers").append('<th> Recargo </th>');
                $("#headers").append('<th> Pago cargo </th>');
                $("#headers").append('<th> Pago recargo </th>');
                $("#headers").append('<th> Total </th>');

                $("#headers").append('<th> Recibo </th>');
                $("#headers").append('<th> Grupo </th>');


                for (var i = 0; i < data.length; i++) {
                    $("#dataColumns").append('<tr><td> '+data[i].cicloa+ '</td> <td> '+data[i].subcicloa+ '</td> <td> '+data[i].matricula+ '</td> <td> '+data[i].date+ '</td> <td> '+data[i].concepto+ '</td><td> '+data[i].subconcepto+ '</td><td> '+(data[i].tipo == 'P' ? "Pago" : "Condonación")+ '</td><td> '+data[i].formapago+ '</td><td> '+data[i].referencia+ '</td><td> '+data[i].amount+ '</td><td> '+data[i].recargo+ '</td><td> '+data[i].pagocargo+ '</td><td> '+data[i].pagorecargo+ '</td><td> ' + (parseInt(data[i].pagocargo) + parseInt(data[i].pagorecargo) )+ '</td><td> '  +data[i].recibo+ '</td><td> '+data[i].grupo+ '</td></tr>');

                }
                console.log(data);
            
                $('#exampleModal').modal({
                    backdrop: 'static'
                  });

             }else{
               alert("No se encontraron datos de pagos");
             }
         })
         .fail(function() {
             console.log("error");
         });
 }

 function concepts(id, matricula, campus){
     $.ajax({
             url: '/getConcetps',
             type: 'POST',
             dataType: 'json',
             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             data: {id: id , matricula: matricula, campus: campus},
         })
         .done(function(data) {
            $(".modal-dialog").css("max-width","100%");								
            $("#headers, #dataColumns, #title-modal ").empty();

            $("#title-modal").text("Conceptos");

            if(data.length){
                $("#headers").append('<th> Ciclo </th>');
                $("#headers").append('<th> Subciclo </th>');
                $("#headers").append('<th> Concepto </th>');
                $("#headers").append('<th> Subconcepto </th>');
                $("#headers").append('<th> Importe </th>');

                for (var i = 0; i < data.length; i++) {

                    $("#dataColumns").append('<tr><td> '+ (data[i].ciclo == undefined ? data[i].cycle_year : data[i].ciclo )+ '</td> <td> '+(data[i].subciclo == undefined ? data[i].cycle_quarter : data[i].subciclo )+ '</td> <td> '+data[i].key+ '</td> <td> '+data[i].clave+ '</td> <td> '+data[i].to_import+ '</td></tr>');

                }
            
                $('#exampleModal').modal({
                    backdrop: 'static'
                  });

             }else{
               alert("No se encontraron conceptos");
             }
         })
         .fail(function() {
             console.log("error");
         });  
 }

 function becasIndv(matricula,campus){
     $.ajax({
             url: '/getBecasIndv',
             type: 'POST',
             dataType: 'json',
             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             data: {matricula: matricula, campus: campus},
         })
         .done(function(data) {

            $(".modal-dialog").css("max-width","100%");								
            $("#headers, #dataColumns, #title-modal ").empty();

            $("#title-modal").text("Becas por alumno");

            if(data.length){
                $("#headers").append('<th> Ciclo </th>');
                $("#headers").append('<th> Subciclo </th>');
                $("#headers").append('<th> Becas </th>');
                $("#headers").append('<th> Descripción Becas </th>');
                $("#headers").append('<th> Concepto </th>');
                $("#headers").append('<th> Subconcepto </th>');
                $("#headers").append('<th> Descuento </th>');

                for (var i = 0; i < data.length; i++) {

                    $("#dataColumns").append('<tr><td> '+data[i].cicloa+ '</td> <td> '+data[i].subcicloa+ '</td> <td> '+data[i].beca+ '</td> <td> '+data[i].concept+ '</td> <td> '+data[i].key+ '</td><td> '+data[i].clave+ '</td><td> '+data[i].discount+ '</td></tr>');

                }
            
                $('#exampleModal').modal({
                    backdrop: 'static'
                  });

             }else{
               alert("No se encontraron becas individuales");
             }

         })
         .fail(function() {
             console.log("error");
     });  
 }

 function becasGroup(matricula,campus){
     $.ajax({
             url: '/getBecasGroup',
             type: 'POST',
             dataType: 'json',
             headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
             data: {matricula: matricula, campus: campus},
         })
         .done(function(data) {
            $(".modal-dialog").css("max-width","100%");								
            $("#headers, #dataColumns, #title-modal ").empty();

            $("#title-modal").text("Becas por Grupo");

            if(data.length){
                $("#headers").append('<th> Ciclo </th>');
                $("#headers").append('<th> Subciclo </th>');
                $("#headers").append('<th> Becas </th>');
                $("#headers").append('<th> Descripción Becas </th>');
                $("#headers").append('<th> Concepto </th>');
                $("#headers").append('<th> Subconcepto </th>');
                $("#headers").append('<th> Descuento </th>');

                for (var i = 0; i < data.length; i++) {

                    $("#dataColumns").append('<tr><td> '+data[i].cicloa+ '</td> <td> '+data[i].subcicloa+ '</td> <td> '+data[i].beca+ '</td> <td> '+data[i].concept+ '</td> <td> '+data[i].key+ '</td><td> '+data[i].clave+ '</td><td> '+data[i].discount+ '</td></tr>');

                }
            
                $('#exampleModal').modal({
                    backdrop: 'static'
                  });

             }else{
               alert("No se encontraron becas grupales");
             }
         })
         .fail(function() {
             console.log("error");
     });  
 }
