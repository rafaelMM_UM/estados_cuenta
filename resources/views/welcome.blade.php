<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="stylesheet" href="{{ ('/css/app.css') }}">


        <title>Laravel</title>
        <!-- Fonts -->
    </head>
    <body>
    <br>

    <label for="alterna">Buscar por alterna</label>
    <input type="number" id="alterna" name="alterna" min="5">

    <select name="campus_alterna" id="campus_alterna">
    @foreach($campuses as $campus)
        <option value="{{$campus->id}}"> {{$campus->name}} </option>
    @endforeach
    </select>

    <input type="button" class="btn btn-primary" value="Buscar por alterna" id="searchAlterna">

    <br><br>

    <table class="table table-hover table-bordered ">
        <thead>

            <th >Nombre</th>
            <th >Apellido Paterno</th>
            <th >Apellido Materno</th>

        </thead>
        <tbody id="resultStudent"> 
        </tbody>
    </table>

    <br><br>
    <br><br>


    <label for="name">Nombre</label>
    <input type="text" id="name" name="name">

    <label for="last_name">Apellido Paterno</label>
    <input type="text" id="last_name" name="last_name">

    <label for="second_lastname">Apellido Materno</label>
    <input type="text" id="second_lastname" name="second_lastname">

    <select name="campus_id" id="campus_id">
    @foreach($campuses as $campus)
        <option value="{{$campus->id}}"> {{$campus->name}} </option>
    @endforeach
    </select>



    <input type="button" class="btn btn-primary" value="Buscar Alumnos" id="search">

    <br><br>



    <table class="table table-hover table-bordered ">
        <thead>
            <th width="7%">Id</th>
            <th width="7%">Matricula</th>
            <th width="15%">Nombre</th>
            <th width="15%">Apellido Paterno</th>
            <th width="15%">Apellido Materno</th>
            <th width="10%">Campus</th>
            <th width="28%">Acciones</th>
        </thead>
        <tbody id="resultDataStudent"> 
        </tbody>
    </table>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="opacity:1">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="title-modal">
                
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="body-modal">
                <table class="table table-hover table-bordered">
                    <thead id="headers">
                    </thead>
                    <tbody id="dataColumns">
                    </tbody>
                </table>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>


    </body>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script src="/js/scripts.js"></script>
</html>
