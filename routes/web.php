<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'AccountController@index');

Route::post('/getdatastudent','AccountController@getdatastudent');

Route::post('/getstudentalterna', 'AccountController@getStudentAlterna');

Route::post('/getstudent_leave_school','AccountController@getstudent_leave_school');

Route::post('/getpayments', 'AccountController@getpayments');

Route::post('/getConcetps', 'AccountController@getConcetps');

Route::post('/getBecasIndv', 'AccountController@getBecasIndv');

Route::post('/getBecasGroup', 'AccountController@getBecasGroup');

Route::get('/exportar/{id}/{student_id}', 'AccountController@view');

Route::get('/exportExcel', 'AccountController@exportExcel');